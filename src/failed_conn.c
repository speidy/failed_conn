#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>

#include <net/ethernet.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <netinet/tcp.h>

#include <arpa/inet.h>

#include <pcap.h>
#include <pcap/sll.h>

/* hashtable implementation grabbed from https://troydhanson.github.io/uthash/ */
#include "uthash/include/uthash.h"


struct connection
{
    uint32_t key;

    uint32_t src_ip;
    uint16_t src_port;
    uint32_t dst_ip;
    uint16_t dst_port;
    u_char tcp_flags;

    UT_hash_handle hh;
};

/* hashtable for holding tcp connections */
static struct connection *conns = NULL;


static void print_connection(struct connection *conn)
{
    struct in_addr src_ip_addr = {
        .s_addr = conn->src_ip
    };
    struct in_addr dst_ip_addr = {
        .s_addr = conn->dst_ip
    };

    printf("src: %s:%d\t", inet_ntoa(src_ip_addr), ntohs(conn->src_port));
    printf("dst: %s:%d\t", inet_ntoa(dst_ip_addr), ntohs(conn->dst_port));
    printf("flags: 0x%01x\n", conn->tcp_flags);
    fflush(0);
}

/* remove and free all connections from `conns` hashtable */
static void delete_all_connections()
{
  struct connection *curr_conn, *tmp;

  HASH_ITER(hh, conns, curr_conn, tmp)
  {
    HASH_DEL(conns, curr_conn);
    free(curr_conn);
  }
}

/* print connections which was dropped (SYN sent, but no response from peer) */
static void print_dropped_connections()
{
  struct connection *curr_conn, *tmp;

  HASH_ITER(hh, conns, curr_conn, tmp)
  {
    if (curr_conn->tcp_flags == TH_SYN)
    {
        print_connection(curr_conn);
    }
  }
}

static void add_new_connection(uint32_t key,
    uint32_t src_ip,
    uint16_t src_port,
    uint32_t dst_ip,
    uint16_t dst_port,
    u_char tcp_flags)
{
    struct connection *new_conn = malloc(sizeof(*new_conn));
    if (NULL == new_conn)
    {
        perror("new_conn");
        return;
    }

    new_conn->key = key;
    new_conn->src_ip = src_ip;
    new_conn->src_port = src_port;
    new_conn->dst_ip = dst_ip;
    new_conn->dst_port = dst_port;
    new_conn->tcp_flags = tcp_flags;

    HASH_ADD_INT(conns, key, new_conn);
}

static void my_packet_handler(u_char *args, const struct pcap_pkthdr *header,
    const u_char *packet)
{
    size_t link_header_size = *(size_t *)args;

    struct ip *ip = (struct ip *)(packet + link_header_size);
    if (ip->ip_p != IPPROTO_TCP)
    {
        // not a tcp packet, skipping
        return;
    }

    struct tcphdr *tcp = (struct tcphdr *)((u_char *)ip + (4 * ip->ip_hl));

    // calculate a unique key for the 4-tuple tcp connection
    // (while ignoring the connection direction)
    uint32_t key = ip->ip_src.s_addr ^ ip->ip_dst.s_addr ^ tcp->th_sport ^ tcp->th_dport;

    struct connection *conn;
    HASH_FIND_INT(conns, &key, conn);
    if (NULL == conn)
    {
        // connection doesn't exist, add new connection
        add_new_connection(key,
            ip->ip_src.s_addr,
            tcp->th_sport,
            ip->ip_dst.s_addr,
            tcp->th_dport,
            tcp->th_flags);
    }
    else
    {
        // connection exits, check if connection got closed by peer (got RST,ACK after SYN)
        if (conn->tcp_flags == TH_SYN && tcp->th_flags == (TH_RST | TH_ACK))
        {
            conn->tcp_flags = tcp->th_flags;
            print_connection(conn);
        }

        // update its tcp flags
        conn->tcp_flags = tcp->th_flags;
    }

}

int main(int argc, char const *argv[])
{
    if (argc != 2)
    {
        printf("usage: %s <pcap file>\n", argv[0]);
        return 1;
    }

    const char *pcap_file = argv[1];
    char errbuf[PCAP_ERRBUF_SIZE];
    pcap_t *p;

    p = pcap_open_offline(pcap_file, errbuf);
    if (NULL == p)
    {
        printf("%s\n", errbuf);
        return 2;
    }

    size_t link_header_size;
    switch (pcap_datalink(p))
    {
    case DLT_LINUX_SLL:
        link_header_size = SLL_HDR_LEN;
        break;

    case DLT_EN10MB:
        link_header_size = ETHER_HDR_LEN;
        break;

    default:
        printf("Unsupported link type.\n");
        return 3;
    }

    printf("processing pcap file...\n");
    int err = pcap_loop(p, 0, my_packet_handler, (u_char *)&link_header_size);
    if (err)
    {
        printf("An error occured while reading %s\n", pcap_file);
    }

    print_dropped_connections();

    delete_all_connections();

    pcap_close(p);

    return 0;
}
